# Excercise

## Installing

Before you install dependencies set up correct node version by calling:

```CLI
nvm use
```

Once `nvm` will setup node version, specified in `.nvmrc` file, dependencies can be installed with:

```CLI
npm install
```

Developers used VSC as a IDE, You will find settings for it in code. You should also use VSC with two extensions that make writing a little bit more easy with project setup:

- Prettier - Code formatter
- Stylelint

<p>&nbsp;</p>

## Available Scripts

In the project directory, you can run:

<p>&nbsp;</p>

### `npm run dev`

Runs [`npm run start`](###npm-run-start) and [`npm run start:mockAPI`](###npm-run-start:mockAPI) in parallel.

<p>&nbsp;</p>

### `npm run start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits, also you will see any lint errors in the console.

<p>&nbsp;</p>

### `npm run start:mockAPI`

Runs node.js express server with mocked API data on [http://localhost:19800](http://localhost:19800)\
(default port can be overriden in [.env](./.env) if You do so remember to also change REACT_APP_API_URL there)

**Note**: As we removed postinstall `cd mockedApi && npm install` in order to use mockedApi You need to do it manually
