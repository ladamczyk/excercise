const jsonServer = require('json-server');
const server = jsonServer.create();
const middlewares = jsonServer.defaults();

const ssoData = require('./response/ssoData.json');

const port = 19800;
const path = '/excersise-api';

server.use(middlewares);
server.use(jsonServer.bodyParser);

server.get('/echo', (req, res) => {
  res.jsonp(req.query);
});

server.get(`/auth-service/api/v2/token/getTokenBySSO`, async (req, res) => {
  res.setTimeout(1000, function () {
    res.jsonp(ssoData);
  });
});

server.listen(port, () => {
  console.log('JSON Server is running ' + port);
});
