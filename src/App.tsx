/* eslint-disable react/no-unescaped-entities */
import { FC } from 'react';

const App: FC = () => {
  return (
    <div>
      <h1>Excercise</h1>

      <p>
        Purpose of this tasks is to find out how good and fast can You produce few basic concepts.
        You can use any tools You like, any docs, stackoverflow etc. Remeber to use MoSCoW rule here
        as follows:
        <ul>
          <li>M: code works and it's buildable (via `npm run build`)</li>
          <li>S: code is reusable, extendible, easy to read</li>
          <li>C: code is tested (via `npm run test`)</li>
          <li>W: CSS styling</li>
        </ul>
      </p>
      <hr />

      <h2>Issue 1 - Payload helper</h2>
      <p>
        Create a helper that will build a payload object from dummy data JSON (./dummy.json)
        depending on selected country.
        <ol>
          <li>
            Helper should support only Poland and England identified by symbols PL and EN. Passing
            any other country symbol produces an error
          </li>
          <li>Each country shares `showSource` method that returns input data</li>
          <li>
            Each country must have `getPayload` method that returns sorted `dummyArray` key, for PL
            ascending for EN descending
          </li>
          <li>Use TS, classes, design patterns etc</li>
        </ol>
      </p>
      <h3>Solution:</h3>
      <p>Display results here for PL and EN `showSource` and `getPayload`</p>
      <hr />

      <h2>Issue 2 - Session length counter</h2>
      <p>
        Create a component that will session length counter to logged user. As soon as counter is
        below 1 minute make counter red.
        <ol>
          <li>
            Fetch user session data from mockedApi (/auth-service/api/v2/token/getTokenBySSO), use
            `expires_in` from response to create counter.
          </li>
          <li>Counter should be decreased each second</li>
          <li>
            When time remaining elapses it should alert 'session expired' and stops displaying
            counter
          </li>
        </ol>
      </p>
      <h3>Solution:</h3>
      <p>Import component here for demo purposes</p>
      <hr />

      <h2>Issue 3 - User input</h2>
      <p>
        Create a input that will log to the console changed value only if users types chars slower
        than 1s between each.
      </p>
      <h3>Solution:</h3>
      <p>Import component here for demo purposes</p>
    </div>
  );
};

export default App;
