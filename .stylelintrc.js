const packageJson = require('./package.json');

module.exports = {
  extends: [
    'stylelint-config-standard-scss',
    'stylelint-config-idiomatic-order',
    'stylelint-prettier/recommended',
  ],
  plugins: [
    'stylelint-scss',
    '@namics/stylelint-bem',
    'stylelint-no-unresolved-module',
    'stylelint-no-unsupported-browser-features',
  ],
  rules: {
    'plugin/stylelint-bem-namics': {
      patternPrefixes: [],
      helperPrefixes: [],
    },
    'plugin/no-unresolved-module': true,
    'at-rule-no-unknown': null,
    'selector-class-pattern': '^.[a-z]([a-z0-9-]+)?(__([a-z0-9]+-?)+)?(--([a-z0-9]+-?)+){0,2}$',
    'plugin/no-unsupported-browser-features': [
      true,
      {
        browsers: packageJson.browserslist,
        ignorePartialSupport: true,
      },
    ],
  },
};
