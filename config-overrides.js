const { alias, aliasJest, configPaths } = require('react-app-rewire-alias');

const aliasMap = configPaths('./tsconfig.paths.json'); // or jsconfig.paths.json

module.exports = function override(config, _env) {
  // we can add something here
  const modifiedConfig = alias(aliasMap)(config);

  return modifiedConfig;
};

module.exports.jest = aliasJest(aliasMap);
